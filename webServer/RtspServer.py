import sys
import cv2
import time
import logging
import os
import urllib.parse

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

from flask import Flask, render_template, Response

from common.CaptureRtspFolderMonitor import CaptureRtspFolderMonitor
from common.RtspServerConfig import RtspServerConfig
from common import FolderSetup


app = Flask(__name__)

config = None

folderMonitor = CaptureRtspFolderMonitor()

observers = []
eventHandlers = {}
logger = None

def main():
    try:
        config = RtspServerConfig(logger)
        logger.info("Starting RtspServer - initializing video capture folder paths")
        for rtspCamera in config.cameras:
            FolderSetup.setup(logger, config.videoCaptureRootPath, rtspCamera.name, False)

        logger.info("Starting RtspServer - folder monitoring")
        folderMonitor.start(config, logger, eventHandlers, observers)

        logger.info("Starting RtspServer - web server")
        app.run(host='0.0.0.0', port=config.webServerPort)

    except KeyboardInterrupt:
        logger.info("RtspServer keyboard interrupt")
        folderMonitor.stop(observers)

    except Exception as ex:
        logger.error("Error in RtspServer.main: %s", str(ex))

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/<cameraName>/video_feed')
def video_feed(cameraName):
    cameraNameLower = urllib.parse.unquote(cameraName.lower())

    if cameraNameLower in eventHandlers:
        return Response(gen(cameraNameLower), mimetype='multipart/x-mixed-replace; boundary=frame')
    else:
        return None

def gen(cameraNameLowerCased):
    currentFile = ""
    
    while True:
        time.sleep(0.1)

        eventHandler = eventHandlers[cameraNameLowerCased]

        if eventHandler.cap != None:
            try: 
                (flag, frame) = eventHandler.cap.read()

                if flag:
                    if currentFile != eventHandler.capturedFile:
                        logger.info(f"Current file = {eventHandler.capturedFile}")
                        currentFile = eventHandler.capturedFile

                    encodedFrame = cv2.imencode(".jpg", frame)[1]
                    yield (b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' + bytearray(encodedFrame) + b'\r\n')

            except Exception as ex:
                logger.error (f"RtspServer.gen({cameraNameLowerCased} Exception: %s", str(ex))

if __name__ == '__main__':
    logger = logging.getLogger("CaptureRtspServer")
    logger.setLevel(logging.INFO)
    logger.propagate = False
    fmt = logging.Formatter(fmt="%(asctime)s: %(message)s", datefmt="%d/%m %H:%M:%S")
    streamHandler = logging.StreamHandler(sys.stdout)
    streamHandler.setFormatter(fmt)
    logger.addHandler(streamHandler)
    
    main()



