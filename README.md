# RtspServer
RtspServer is a Python 3 app for converting one or more RTSP camera streams into MJPEG streams which can then be served to a web browser/client. 
  
## Installation and setup

After downloading or cloning this repo, cd into the folder where you downloaded this repo and execute the following commands:

    chmod u+x installDependencies.sh
    chmod u+x startCapture.sh
    chmod u+x startRtspServer.sh
    chmod u+x startServer.sh

Execute the installDependencies script by entering the following command:

    ./installDependencies.sh

This script will download the dependencies for this Python project, including FFMpeg which is used to convert the RTSP stream for your RTSP camera into MJPEG which can then be streamed to a web browser.
Then copy the file RtspServerConfigTemplate.json to the file RtspServerConfig.json with the following command:

    cp RtspServerConfigTemplate RtspServerConfig.json

Edit the file RtspServerConfig.json to specify the root folder that you want all the RTSP camera feed captures to go to, and the list of all RTSP camera's, their names and their RTSP URLs, including username, password and IP address.

For each camera, you can override the default video quality and default video scaling if you specify camera specific values for quality and scale respectively.

## RtspServer components
RtspServer consists primarily of two servers, CaptureRtspServer.py and RtspServer.py.
CaptureRtspServer.py uses FFMpeg to capture the RTSP feed from each camera and then converts it to an MJPEG file under a folder with the given camera's name. 

RtspServer.py monitors each camera's subfolder and upon completion of capturing of a 30 seconds segment of MJPEG video, serves up that file to any web clients. 
 
## Running the server
From the folder where the RtspServer files were downloaded to, execute the following command:

    ./startServer.sh

Or if you prefer to run the components separately:

    ./startCapture.sh
    ./startRtstpServer.sh

To run the video capture/conversion and the MJPEG web server separately.

## Camera streaming Web URL 
The url feed for the MJPEG stream will be as follows:

http://XX.XX.XX.XX:8080/<CAMERA_NAME>/video_feed

Where XX.XX.XX.XX is the local IP address of the box running RtspServer, 8080 is the default port, and CAMERA_NAME is the name of the camera in the RtspServerConfig.json file.




