import os
import logging
from pathlib import Path
import subprocess
import time

import sys
import signal
import psutil

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

from CaptureProcess import CaptureProcess
from common import FolderSetup
from common.RtspServerConfig import RtspServerConfig

_captureRtspService = None

class CaptureRtspService:
    _logger = None
    _captureProcesses = []

    MAX_IDLE_COUNT = 4

    def __init__(self):
        self._logger = logging.getLogger("CaptureRtspService")
        self._logger.setLevel(logging.INFO)
        self._logger.propagate = False
        fmt = logging.Formatter(fmt="%(asctime)s: %(message)s", datefmt="%d/%m %H:%M:%S")
        streamHandler = logging.StreamHandler(sys.stdout)
        streamHandler.setFormatter(fmt)
        self._logger.addHandler(streamHandler)
        
    def isRunning(self):
        return len(self._captureProcesses) > 0

    def start(self):
        config = RtspServerConfig(self._logger)    

        if len(self._captureProcesses) == 0:
            self._killPreexistingFfmpegProcesses()

            self._logger.info ("CaptureRtspSerice starting...")
        
            for cameraSetting in config.cameras:
                self._startCapture(cameraSetting, config)

        while _captureRtspService.isRunning:
             time.sleep(60)

        #     #reload config file in case there have been changes
             config.reloadCameras()

             captureProcesses = self._captureProcesses.copy()

             for cp in captureProcesses:
                 cameraSetting = cp.cameraSetting

                 if not psutil.pid_exists(cp.pid) and self._hasFreeProcess():                 
                    self._captureProcesses.remove(cp)

                    self._startCapture(cameraSetting, config, True)
                 else:
                     process = psutil.Process(cp.pid)                
                     cpuUsage = process.cpu_percent()

                     if (cpuUsage == 0.0):
                        cameraSetting = cp.cameraSetting
                        cameraSetting.cpuUsageZeroCount += 1

                        self._logger.info(f"Capture process PID {cp.pid} idle count = {cameraSetting.cpuUsageZeroCount}")

                        # if CPU usage for camera process is zero after number of checks, restart process
                        if cameraSetting.cpuUsageZeroCount >= self.MAX_IDLE_COUNT:
                            try:
                                self._logger.info(f"Killing idle PID {process.pid} ")
                                process.kill()
                                #os.kill(cp.pid, signal.SIGTERM)
                            except Exception as ex:
                                self._logger.error(f"Error killing process, exception: {str(ex)}")

                            if self._hasFreeProcess():
                                self._logger.info(f"Capture for camera {cameraSetting.name} has stalled, restarting capture process")

                                self._captureProcesses.remove(cp)

                                self._startCapture(cameraSetting, config, True)
                        else:
                            cameraSetting.cpuUsageZeroCount = 0


    def _killPreexistingFfmpegProcesses(self):
         for proc in psutil.process_iter():
            if proc.name().lower() == "ffmpeg":
                try:
                    proc.kill()
                except Exception as ex:
                    self._logger.error(f"Error killing process, exception: {str(ex)}")

    def _hasFreeProcess(self):
        count = 0
        for proc in psutil.process_iter():
            if proc.name().lower() == "ffmpeg":
                count += 1

        self._logger.info(f"Number of ffmpeg processes = {count}")
        return count < len(self._captureProcesses)
                                

    def _startCapture(self, cameraSetting, config, isRestart = False):
        try: 
            if cameraSetting.enabled:           
                cameraSetting.cpuUsageZeroCount = 0

                capturePath = FolderSetup.setup(self._logger, config.videoCaptureRootPath, cameraSetting.name, not isRestart)

                captureFilepath = os.path.join(capturePath, "%Y%m%d_%H%M%S.mjpeg")
                ffmpeg_cli = "ffmpeg -i {cameraUrl} -c:v mjpeg -vf scale={scale} -q:v {quality} -an -map 0 -f segment -strftime 1 -segment_time {timeSegment} -segment_format mjpeg -reset_timestamps 1 '{captureFilePath}' 2> /dev/null"\
                    .replace("{cameraUrl}",cameraSetting.url)\
                    .replace("{scale}", config.defaultScale)\
                    .replace("{quality}", str(config.defaultQuality))\
                    .replace("{timeSegment}", str(config.defaultTimeSegment))\
                    .replace("{captureFilePath}", captureFilepath)
    

                pid = subprocess.Popen(ffmpeg_cli, shell=True).pid + 1

                if isRestart:
                    self._logger.info(f"Restart capture RTSP from {cameraSetting.name} and converting to MJPEG at {cameraSetting.scale}, quality={cameraSetting.quality} , PID = {pid}")
                else:
                    self._logger.info(f"Capturing RTSP from {cameraSetting.name} and converting to MJPEG at {cameraSetting.scale}, quality={cameraSetting.quality}, PID = {pid}")

                
                self._captureProcesses.append(CaptureProcess(pid, cameraSetting))
                
            
        except Exception as ex:
            self._logger.error (f"CaptureRtspService.start({cameraSetting.name}): Exception: {str(ex)}")


    def stop(self):
        if len(self._captureProcesses) > 0:
            self._logger.info ("CaptureRtspService stopping...")

            for cp in self._captureProcesses:
                try:
                    psutil.Process(cp.pid).kill()
                except Exception as ex:
                    self._logger.error(f"Error killing process, exception: {str(ex)}")

                self._captureProcesses.remove(cp)


def handler(signum, frame):
    res = input("Ctrl-c was pressed. Do you really want to exit? y/n ")
    if res == 'y':
        _captureRtspService.stop()
        exit(1)
    

if __name__ == '__main__':      
    _captureRtspService = CaptureRtspService()
    
    signal.signal(signal.SIGINT, handler)
    _captureRtspService.start()


