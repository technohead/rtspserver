import cv2
import logging
import os
from os.path import exists
from pathlib import Path
from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer  #creating an instance of the watchdog.observers.Observer from watchdogs class.
from watchdog.events import LoggingEventHandler


# One WatcherEventHandler per camera
class WatcherEventHandler(FileSystemEventHandler):

    _prevFile = ""
    _newFile = ""
    _logger = None

    capturedFile = ""

    cap = None

    def __init__(self, logger):
        self._logger = logger

    def on_created(self, event): 
        self._prevFile = self.capturedFile 
        self.capturedFile = self._newFile

        self._newFile = event.src_path
        self._logger.info("Capturing new file: %s", self._newFile)

        if self.capturedFile != "" and exists(self.capturedFile):
            self._logger.info("Finished capturing file: %s", self.capturedFile)
            self.openFinishedCapturedFile()

        if self._prevFile != "" and exists(self._prevFile):
            try:
                self._logger.info("Removing previous file: %s", self._prevFile)
                os.remove(self._prevFile)
            except:
                pass

    def openFinishedCapturedFile(self):
        if self.capturedFile != "" and exists(self.capturedFile):
            try:
                # causes segmentation fault
                #if self.cap != None:
                #    self.cap.release()

                self.cap = cv2.VideoCapture(self.capturedFile)
                self._logger.info("Opened captured file: %s", self.capturedFile)                
            except Exception as ex:
                self._logger.error(f"WatcherEventHandler.openFinishedCapturedFile: Exception: {str(ex)}")
 
        

            