import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

from common.WatcherEventHandler import WatcherEventHandler
from common import FolderSetup
from watchdog.observers import Observer

class CaptureRtspFolderMonitor:
    _isRunning = False
    _logger = None

    def start(self, config, logger, eventHandlers, observers):
        self._logger = logger

        if self._isRunning == False:
            try:
                for cameraSetting in config.cameras:
                    capturePath = FolderSetup.setup(logger, config.videoCaptureRootPath, cameraSetting.name)

                    event_handler = WatcherEventHandler(logger)
                    eventHandlers[cameraSetting.name] = event_handler

                    observer = Observer()
                    observers.append(observer)

                    self._logger.info ("Watching path: %s", capturePath)

                    observer.schedule(event_handler, capturePath, recursive=False)  
                    observer.start()  #for starting the observer thread

                self._isRunning = True
            except Exception as ex:
                self._logger.error ("CaptureRtspFolderMonitor.start: Exception: %s", str(ex))        
    
    def stop(self, observers):
        self._logger.info("CaptureRtspFolderMonitor.stop()")

        if self._isRunning == True:
            self._logger.info("CaptureRtspFolderMonitor stopping...")
            for observer in observers:
                observer.stop()

            self._isRunning = False