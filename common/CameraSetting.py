class CameraSetting:
    name = None
    url = None
    scale = None
    quality = None
    enabled = True
    cpuUsageZeroCount = 0

    def __init__(self, name, url, scale, quality, enabled):
        self.name = name.lower()
        self.url = url
        self.scale = scale
        self.quality = quality
        self.enabled = enabled