import os
import logging
import shutil

def setup(logger, videoCaptureRootPath, cameraName, clearFolder = False):

    try:
        if (not os.path.exists(videoCaptureRootPath)):
            logger.info("Creating video capture root path: %s", videoCaptureRootPath)
            os.mkdir(videoCaptureRootPath)
        
        rtspCameraPath = os.path.join(videoCaptureRootPath, cameraName)

        if clearFolder:
            shutil.rmtree(rtspCameraPath)

        if (not os.path.exists(rtspCameraPath)):
            logger.info("Creating camera path: %s", rtspCameraPath)
            os.mkdir(rtspCameraPath)

        return rtspCameraPath

    except Exception as ex:
        logger.error ("FolderSetup.setup(): Exception:%s", str(ex))  
                