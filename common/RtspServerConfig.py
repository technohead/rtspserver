import json
import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

from pathlib import Path

from common.CameraSetting import CameraSetting



class RtspServerConfig:
    DEFAULT_SCALE = "defaultScale"
    DEFAULT_SCALE_VALUE = "-1:540"
    DEFAULT_QUALITY = "defaultQuality"
    DEFAULT_QUALITY_VALUE = 10
    DEFAULT_TIME_SEGMENT_SECS = "captureTimeSegmentSecs"
    DEFAULT_TIME_SEGMENT_SECS_VALUE = 30
    
    VIDEO_CAPTURE_ROOT_PATH = "videoCaptureRootPath"
    WEBSERVER_PORT = "webServerPort"
    WEBSERVER_PORT_VALUE = 8080

    RTSP_CAMERAS = "rtspCameras"
    NAME = "name"
    RTSP_URL = "rtspUrl"
    SCALE = "scale"
    QUALITY = "quality"
    ENABLED = "enabled"

    webServerPort = None

    defaultTimeSegment = None
    defaultScale = None
    defaultQuality = None
    rtspCameras = None
    videoCaptureRootPath = None

    cameras = []
    _logger = None


    def __init__(self, logger):
        self._logger = logger

        with open('RtspServerConfig.json') as config_file:
            config = json.load(config_file)

            self.webServerPort = self.WEBSERVER_PORT_VALUE
            if self.WEBSERVER_PORT in config:
                self.webServerPort = config[self.WEBSERVER_PORT]

            self.videoCaptureRootPath = str(Path(config[self.VIDEO_CAPTURE_ROOT_PATH]).expanduser())
            self._logger.info("videoCaptureRootPath = %s", self.videoCaptureRootPath)

            if self.DEFAULT_TIME_SEGMENT_SECS in config:
                self.defaultTimeSegment = config[self.DEFAULT_TIME_SEGMENT_SECS]
            else:
                self.defaultTimeSegment = self.DEFAULT_TIME_SEGMENT_SECS_VALUE
            
            if self.DEFAULT_QUALITY in config:
                self.defaultQuality = config[self.DEFAULT_QUALITY]
            else:
                self.defaultQuality = self.DEFAULT_QUALITY_VALUE

            if self.DEFAULT_SCALE in config:
                self.defaultScale = config[self.DEFAULT_SCALE]
            else:
                self.defaultScale = self.DEFAULT_SCALE_VALUE


            self._loadCameraSettings(config)
               

    def _loadCameraSettings(self, config):
        self.cameras.clear()

        for rtspCamera in config[self.RTSP_CAMERAS]:
            try: 
                cameraName = rtspCamera[self.NAME]
                url = rtspCamera[self.RTSP_URL]

                scale = self.defaultScale
                quality = self.defaultQuality
                enabled = True

                if self.ENABLED in rtspCamera:
                    enabled = rtspCamera[self.ENABLED]
                
                if self.SCALE in rtspCamera:
                    scale = rtspCamera[self.SCALE]

                if self.QUALITY in rtspCamera:
                    quality = rtspCamera[self.QUALITY]

                self.cameras.append(CameraSetting(cameraName, url, scale, quality, enabled))
            except Exception as ex:
                self._logger.error (f"RtspServerConfig error parsing ({cameraName}): Exception: {str(ex)}")

    def reloadCameras(self):
        with open('RtspServerConfig.json') as config_file:
            config = json.load(config_file)

            self._loadCameraSettings(config)

